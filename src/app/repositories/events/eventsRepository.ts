import { injectable } from 'inversify';
import { transaction } from 'objection';
import { IEvent, IFindEvent, EventModel} from '../../models';
import { GenericResponseError, HttpStatusCode } from '../../utils';

@injectable()
export class EventRepository {
  public async createEvent(data: IEvent): Promise<IEvent> {
    try {
      return await transaction(EventModel, async (EventModel) => {
        return EventModel.query().insertGraphAndFetch(data);
      });
    } catch (e) {
      e.code = e.code || HttpStatusCode.INTERNAL_SERVER_ERROR;
      throw new GenericResponseError(e.message, e.code);
    }
  };

  public async findOne(data: IFindEvent): Promise<IEvent> {
    try {
      return await transaction(EventModel, async (EventModel) => {
        return EventModel.query().where(data).withGraphFetched(`[actors, repos]`)
        .first();
      });
    } catch (e) {
      if (typeof e.code === 'string' || !e.code) {
        e.code = HttpStatusCode.INTERNAL_SERVER_ERROR;
      }
      throw new GenericResponseError(e.message, e.code);
    }
  };

  public async findEventById(id: string): Promise<IEvent> {
    try {
      return await transaction(EventModel, async (EventModel) => {
        return EventModel.query().findById(id).withGraphFetched(`[actors, repos]`);
      });
    } catch (e) {
      throw new GenericResponseError(e.message, e.code);
    }
  };

  public async findAllEvents(): Promise<IEvent[]> {
    try {
      return await transaction(EventModel, async (EventModel) => {
        return EventModel.query().withGraphFetched(`[actors, repos]`);
      });
    } catch(e) {
      throw new GenericResponseError(e.message, e.code);
    }
  };

  public async updateEventById(id: string, data: IFindEvent): Promise<IEvent> {
    try{
      return await transaction(EventModel, async (EventModel) => {
        return EventModel.query().patchAndFetchById(id, data);
      });
    }
    catch(e) {
      throw new GenericResponseError(e.message, e.code);
    }
  };
}
