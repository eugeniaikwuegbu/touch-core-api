import { injectable } from 'inversify';
import { transaction } from 'objection';
import { IActor, IFindActor, ActorModel} from '../../models';
import { GenericResponseError, HttpStatusCode } from '../../utils';

@injectable()
export class ActorRepository {
  public async createActor(data: IActor): Promise<IActor> {
    try {
      return await transaction(ActorModel, async (ActorModel) => {
        return ActorModel.query().insertGraphAndFetch(data);
      });
    } catch (e) {
      e.code = e.code || HttpStatusCode.INTERNAL_SERVER_ERROR;
      throw new GenericResponseError(e.message, e.code);
    }
  };

  public async findOneActor(data: IFindActor): Promise<IActor> {
    try {
      return await transaction(ActorModel, async (ActorModel) => {
        return ActorModel.query().where(data).first();
      });
    } catch (e) {
      if (typeof e.code === 'string' || !e.code) {
        e.code = HttpStatusCode.INTERNAL_SERVER_ERROR;
      }
      throw new GenericResponseError(e.message, e.code);
    }
  }

  public async findActorById(id: string): Promise<IActor> {
    try {
      return await transaction(ActorModel, async (ActorModel) => {
        return ActorModel.query().findById(id);
      });
    } catch (e) {
      throw new GenericResponseError(e.message, e.code);
    }
  };

  public async updateActorById(id: string, data: IFindActor): Promise<IActor> {
    try{
      return await transaction(ActorModel, async (ActorModel) => {
        return ActorModel.query().patchAndFetchById(id, data);
      });
    }
    catch(e) {
      throw new GenericResponseError(e.message, e.code);
    }
  }
}
