import { injectable } from 'inversify';
import { transaction } from 'objection';
import { IRepo, IFindRepo, RepoModel} from '../../models';
import { GenericResponseError, HttpStatusCode } from '../../utils';

@injectable()
export class RepoRepository {
  public async createRepo(data: IRepo): Promise<IRepo> {
    try {
      return await transaction(RepoModel, async (RepoModel) => {
        return RepoModel.query().insertGraphAndFetch(data);
      });
    } catch (e) {
      e.code = e.code || HttpStatusCode.INTERNAL_SERVER_ERROR;
      throw new GenericResponseError(e.message, e.code);
    }
  };

  public async findOne(data: IFindRepo): Promise<IRepo> {
    try {
      return await transaction(RepoModel, async (RepoModel) => {
        return RepoModel.query().where(data).first();
      });
    } catch (e) {
      if (typeof e.code === 'string' || !e.code) {
        e.code = HttpStatusCode.INTERNAL_SERVER_ERROR;
      }
      throw new GenericResponseError(e.message, e.code);
    }
  }

  public async findRepoById(id: string): Promise<IRepo> {
    try {
      return await transaction(RepoModel, async (RepoModel) => {
        return RepoModel.query().findById(id).first();
      });
    } catch (e) {
      throw new GenericResponseError(e.message, e.code);
    }
  };

  public async updateRepoById(id: string, data: IFindRepo): Promise<IRepo> {
    try{
      return await transaction(RepoModel, async (RepoModel) => {
        return RepoModel.query().patchAndFetchById(id, data);
      });
    }
    catch(e) {
      throw new GenericResponseError(e.message, e.code);
    }
  };
}
