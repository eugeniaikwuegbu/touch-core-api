import {inject, injectable} from "inversify";
import TYPES from "../../config/types";
import {IActor, IFindActor } from "../../models";
import {ActorRepository} from "../../repositories";
import { GenericResponseError, HttpStatusCode, throwError } from "../../utils";

@injectable()
export class ActorService {
    @inject(TYPES.ActorRepository)
    private readonly actorRepo: ActorRepository;

    public async createActor(data: IActor): Promise<any> {
        try{
            return await this.actorRepo.createActor(data)

        }catch(e) {
            throw new GenericResponseError(e.message, HttpStatusCode.INTERNAL_SERVER_ERROR);
        }
    };

    public async findById(id: string): Promise<any> {
        try {
            const actor: IActor = await this.actorRepo.findActorById(id);
            if(!actor) {
                throw new GenericResponseError('Actor not found', HttpStatusCode.NOT_FOUND);
            }
            return {
                data: actor
            }
        } catch(e) {
            throw new GenericResponseError(e.message, HttpStatusCode.INTERNAL_SERVER_ERROR);
        }
    };

    public async findOneActor(data: IFindActor): Promise<any> {
        try {
            return await this.actorRepo.findOneActor(data)
        } catch(e) {
            throw new GenericResponseError(e.message, HttpStatusCode.INTERNAL_SERVER_ERROR);
        }
    };

    public async findAndUpdate(id:string,data: IFindActor): Promise<any> {
        try {
            return await this.actorRepo.updateActorById(id,data)
        } catch(e) {
            throw new GenericResponseError(e.message, HttpStatusCode.INTERNAL_SERVER_ERROR);
        }
    };

}