import {inject, injectable} from "inversify";
import TYPES from "../../config/types";
import { IFindRepo, IRepo } from "../../models";
import { RepoRepository } from "../../repositories";
import { GenericResponseError, HttpStatusCode } from "../../utils";

@injectable()
export class RepoService {
    @inject(TYPES.RepoRepository)
    private readonly repoRepo: RepoRepository;

    public async createRepo(data: IRepo): Promise<any> {
        try{
            return await this.repoRepo.createRepo(data);
        }catch(e) {
            throw new GenericResponseError(e.message, HttpStatusCode.INTERNAL_SERVER_ERROR);
        }
    };

    public async findOneRepo(data: IFindRepo): Promise<any> {
        try{
            return await this.repoRepo.findOne(data);
        }catch(e) {
            throw new GenericResponseError(e.message, HttpStatusCode.INTERNAL_SERVER_ERROR);
        }
    };

    public async findById(id: string): Promise<any> {
        try{
            return await this.repoRepo.findRepoById(id);
        }catch(e) {
            throw new GenericResponseError(e.message, HttpStatusCode.INTERNAL_SERVER_ERROR);
        }
    };

    public async updateRepo(id: string, data: IFindRepo): Promise<any> {
        try{
            return await this.repoRepo.updateRepoById(id,data);
        }catch(e) {
            throw new GenericResponseError(e.message, HttpStatusCode.INTERNAL_SERVER_ERROR);
        }
    }
}