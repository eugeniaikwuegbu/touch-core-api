import {inject, injectable} from "inversify";
import TYPES from "../../config/types";
import {IActor, IEvent, IRepo} from "../../models";
import { EventRepository } from "../../repositories";
import {error, GenericResponseError, HttpStatusCode, throwError} from "../../utils";

@injectable()
export class EventService {
    @inject(TYPES.EventRepository)
    private readonly eventRepo: EventRepository;

    public async createEvent(data: IEvent): Promise<any> {
        try{

            const repos: IRepo  = {
                repo_name: '',
                repo_url: ''
            }
            const actors: IActor = {
                login: '',
                avatar_url: ''
            }
            const event: IEvent = {
                ...data,
                repos,
                actors
            }
           return await this.eventRepo.createEvent(event);

        }catch(e) {
            throw new GenericResponseError(e.message, HttpStatusCode.INTERNAL_SERVER_ERROR);
        }
    };

    public async findAllEvents(): Promise<any> {
        try {
            const allEvents =  await this.eventRepo.findAllEvents();
            return allEvents.sort((a: any,b :any) => {
                if (a.id > b.id) {
                    return 1
                }

                if (b.id > a.id) {
                    return -1
                }

                return 0;
            });
        } catch(e) {
            throw new GenericResponseError(e.message, HttpStatusCode.INTERNAL_SERVER_ERROR);
        }
    }

    public async findById(id: string): Promise<any> {
        try{
            // finding actors by id
            const allEvents = await this.eventRepo.findAllEvents();
            const filteredEvents = allEvents.filter((event) => event.actor_id === id)

            if(!filteredEvents) {
                throwError('Actor does not exist', error.notFound);
            }
            return allEvents.sort((a: any,b :any) => {
                if (a.id > b.id) {
                    return 1
                }

                if (b.id > a.id) {
                    return -1
                }
                    return 0;
            });
        }catch(e) {
            throw new GenericResponseError(e.message, HttpStatusCode.INTERNAL_SERVER_ERROR);
        }
    };

    // public async findOneEvent(data: IFindEvent): Promise<any> {
    //     try{
    //         return await this.eventRepo.findOne(data);
    //     }catch(e) {
    //         throw new GenericResponseError(e.message, HttpStatusCode.INTERNAL_SERVER_ERROR);
    //     }
    // };
    //
    // public async updateOneEvent(id: string, data: IFindEvent): Promise<any> {
    //     try{
    //         return await this.eventRepo.updateEventById(id, data);
    //     }catch(e) {
    //         throw new GenericResponseError(e.message, HttpStatusCode.INTERNAL_SERVER_ERROR);
    //     }
    // };
}