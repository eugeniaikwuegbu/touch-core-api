const TYPES = {
  // controllers
  ActorController: Symbol('ActorController'),
  EventController: Symbol('EventController'),
  RepoController: Symbol('RepoController'),


  // service
  ActorService: Symbol('ActorService'),
  EventService: Symbol('EventService'),
  RepoService: Symbol('RepoService'),


  // repositories
  ActorRepository: Symbol('ActorRepository'),
  EventRepository: Symbol('EventRepository'),
  RepoRepository: Symbol('RepoRepository'),
};

export default TYPES;
