import 'reflect-metadata';
import { Container } from 'inversify';
import TYPES from './types';
import { ActorRepository, EventRepository, RepoRepository } from '../repositories';
import {EventController, RepoController, ActorController} from "../controllers";
import { EventService, ActorService, RepoService} from "../services";

const container = new Container();

// controllers
container
    .bind<ActorController>(TYPES.ActorController)
    .to(ActorController)
    .inSingletonScope();
container
    .bind<EventController>(TYPES.EventController)
    .to(EventController)
    .inSingletonScope();
container
    .bind<RepoController>(TYPES.RepoController)
    .to(RepoController)
    .inSingletonScope();
 

// services
container
    .bind<ActorService>(TYPES.ActorService)
    .to(ActorService)
    .inSingletonScope();
container
    .bind<EventService>(TYPES.EventService)
    .to(EventService)
    .inSingletonScope();
container
    .bind<RepoService>(TYPES.RepoService)
    .to(RepoService)
    .inSingletonScope();


// repositories
container
    .bind<ActorRepository>(TYPES.ActorRepository)
    .to(ActorRepository)
    .inSingletonScope();
container
    .bind<RepoRepository>(TYPES.RepoRepository)
    .to(RepoRepository)
    .inSingletonScope();
container
    .bind<EventRepository>(TYPES.EventRepository)
    .to(EventRepository)
    .inSingletonScope();



export default container;
