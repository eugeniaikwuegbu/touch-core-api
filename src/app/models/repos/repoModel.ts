import { JSONSchema } from 'objection';
import { Schema, Table } from '../../../database';
import { BaseModel } from '../base';
import { IRepo } from './interfaces';
import { RepoValidation } from './validation';

export class RepoModel extends BaseModel implements IRepo {
  public repo_name: IRepo['repo_name'];
  public repo_url: IRepo['repo_url'];

  static get tableName(): string {
    return `${Schema.touchCore}.${Table.repos}`;
  }

  static get jsonSchema(): JSONSchema {
    return RepoValidation;
  }

  static get hidden(): string[] {
    return [];
  }
}
