import { IBase } from "../../base";

export interface IRepo extends IBase {
  repo_name: string
  repo_url: string
}

export interface IFindRepo extends IBase {
  repo_name?: string
  repo_url?: string
}