import { JSONSchema } from 'objection';

export const RepoValidation: JSONSchema = {
  type: 'object',
  title: 'Repo Schema Validation',
  required: ['repo_name', 'repo_url'],

  properties: {
    repo_name: { type: 'string'},
    repo_url: { type: 'string'},
  }
}
