import { IBase } from '../../base';

export interface IActor extends IBase {
  login: string,
  avatar_url: string,

}

export interface IFindActor extends IBase {
  login?: string,
  avatar_url?: string,
}
