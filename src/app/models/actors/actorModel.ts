import { JSONSchema} from 'objection';
import { Schema, Table } from '../../../database';
import { BaseModel } from '../base';
import { IActor } from './interfaces';
import { ActorValidation } from './validation';

export class ActorModel extends BaseModel implements IActor {
  public login: IActor['login'];
  public avatar_url: IActor['avatar_url'];

  static get tableName(): string {
    return `${Schema.touchCore}.${Table.actors}`;
  }

  static get jsonSchema(): JSONSchema {
    return ActorValidation;
  }

  static get hidden(): string[] {
    return [];
  }
}
