import { JSONSchema } from 'objection';

export const ActorValidation: JSONSchema = {
  type: 'object',
  title: 'Actor Schema Validation',
  required: [ 'login', 'avatar_url' ],
  properties: {
    login: {type: 'string'},
    avatar_url: {type: 'string'},
  }
}
