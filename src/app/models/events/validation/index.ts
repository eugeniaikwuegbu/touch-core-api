import { JSONSchema } from 'objection';
import {RepoValidation} from "../../repos";
import {ActorValidation} from "../../actors";

export const EventValidation: JSONSchema = {
  type: 'object',
  title: 'Event Schema Validation',
  required: [ 'event_type', 'actor_id', 'repo_id'],
  properties: {
    event_type: {type: 'string'},
    actor_id: {format: 'uuid'},
    repo_id: {format: 'uuid'},
    repos: {
      ...RepoValidation
    },
    actors: {
      ...ActorValidation
    }
  }
}
