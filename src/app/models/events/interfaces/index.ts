import { IBase } from "../../base";
import {IRepo} from "../../repos";
import {IActor} from "../../actors";

export interface IEvent extends IBase {
  event_type: string;
  repos: IRepo,
  actors: IActor,
  actor_id: string;
  repo_id: string;
}

export interface IFindEvent extends IBase {
  event_type?: string,
  actor_id?: string;
  repo_id?: string;
}