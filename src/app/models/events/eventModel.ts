import { JSONSchema, RelationMappings } from 'objection';
import { Schema, Table } from '../../../database';
import { BaseModel } from '../base';
import { IEvent } from './interfaces';
import { EventValidation } from './validation';

export class EventModel extends BaseModel implements IEvent {
  public event_type: IEvent['event_type'];
  public actor_id: IEvent['actor_id'];
  public repo_id: IEvent['repo_id'];
  public repos: IEvent['repos'];
  public actors: IEvent['actors'];

  static get tableName(): string {
    return `${Schema.touchCore}.${Table.events}`;
  }

  static get jsonSchema(): JSONSchema {
    return EventValidation;
  }

  static get hidden(): string[] {
    return [];
  }

  static get relationMappings(): RelationMappings {
    return {
      repos: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: '../repos',
        join: {
          from: `${Schema.touchCore}.${Table.events}.repo_id`,
          to: `${Schema.touchCore}.${Table.repos}.id`
        }
      },

      actors: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: '../actors',
        join: {
          from: `${Schema.touchCore}.${Table.events}.actor_id`,
          to: `${Schema.touchCore}.${Table.actors}.id`
        }
      },
    };
  }
}
