import { Request, Response } from 'express';
import {
  controller, httpPost,
  request, response,
} from 'inversify-express-utils';
import TYPES from '../../config/types';
import { BaseController } from '../base';
import { inject } from 'inversify';
import { RepoService } from '../../services';
import {IRepo } from '../../models';
import {HttpStatusCode} from '../../utils';


@controller('/repos')
export class RepoController extends BaseController {
  @inject(TYPES.RepoService)
  private readonly repoService: RepoService;

  // posting new repos
  @httpPost('/')
  public async createEvent(@request() req: Request, @response() res: Response) {
    try {
      const repo: IRepo = await this.repoService.createRepo(req.body);
      this.success(res, repo, 'Repo created Successfully', HttpStatusCode.CREATED);
    } catch (e) {
      this.error(res, e);
    }
  }
}
