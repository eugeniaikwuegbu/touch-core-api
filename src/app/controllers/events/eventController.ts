import { Request, Response } from 'express';
import {
  controller, httpPost,
  httpGet,
  httpDelete,
  request,
  response,
} from 'inversify-express-utils';
import TYPES from '../../config/types';
import { BaseController } from '../base';
import { inject } from 'inversify';
import { EventService } from '../../services';
import { IEvent } from '../../models';
import {HttpStatusCode} from '../../utils';


@controller('/events')
export class EventController extends BaseController {
  @inject(TYPES.EventService)
  private readonly eventService: EventService;

  // posting new events
  @httpPost('/')
  public async createEvent(@request() req: Request, @response() res: Response) {
    try {
      const event: IEvent = await this.eventService.createEvent(req.body);
      this.success(res, event, 'Event created Successfully', HttpStatusCode.CREATED);
    } catch (e) {
      this.error(res, e);
    }
  };

// erasing all events
@httpDelete('/erase')
public async deleteAllEvents(@request() req: Request, @response() res: Response) {
  try {
    const deletedEvents: IEvent = await this.eventService.findAllEvents()
    this.success(res, deletedEvents, 'Event deleted successfully', HttpStatusCode.OK)
  } catch (e) {
    this.error(res, e)
  }
};

// get all events
@httpGet('/')
public async getAllEvents(@request() req: Request, @response() res: Response) {
  try {
    const allEvents: IEvent = await this.eventService.findAllEvents()
    this.success(res, allEvents, 'Events retrieved Successfully', HttpStatusCode.OK)
  } catch(e) {
    this.error(res, e);
  }
};

// get events by actor id
@httpGet('/actor/:id')
public async getEventsByActorId(@request() req: Request, @response() res: Response) {
  try {
    const { id } = req.params
    const events: IEvent = await this.eventService.findById(id)
    this.success(res, events, 'Events retrieved Successfully', HttpStatusCode.OK)
  } catch(e) {
    this.error(res, e);
  }
};
}
