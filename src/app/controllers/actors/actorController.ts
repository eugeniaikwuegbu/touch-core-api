import { Request, Response } from 'express';
import {
  controller,
  httpPut,
  request,
  response,
} from 'inversify-express-utils';
import TYPES from '../../config/types';
import { BaseController } from '../base';
import { inject } from 'inversify';
import { ActorService } from '../../services';
import { IActor } from '../../models';
import { HttpStatusCode} from '../../utils';
import { update } from 'lodash';


@controller('/actors')
export class ActorController extends BaseController {
  @inject(TYPES.ActorService)
  private readonly actorService: ActorService;

// update actor avatar url
@httpPut('/:id')
public async updateActorUrl(@request() req: Request, @response() res: Response) {
  try {
    const {id } = req.params;
    const updatedURL: IActor= await this.actorService.findById(id)
    this.success(res, updatedURL, 'Avatar Updated successfully', HttpStatusCode.OK)
  } catch (e) {
    this.error(res, e)
  }
};

}
