import * as Knex from 'knex';
import { Schema } from '../schema';
import { Table } from '../table';

export async function up(knex: Knex): Promise<void> {
  return knex
    .transaction(async (trx: Knex.Transaction) => trx.schema
      .createSchemaIfNotExists(Schema.touchCore)
      .then(() => trx.schema.hasTable(Table.events)
        .then((tableExists: boolean) => {
          if (!tableExists) {
            return trx.schema
              .withSchema(Schema.touchCore)
              .createTable(Table.events, (tableBuilder: Knex.CreateTableBuilder) => {
                tableBuilder
                  .uuid('id')
                  .unique()
                  .notNullable()
                  .defaultTo(knex.raw('gen_random_uuid()'))
                  .primary(`${Table.events}_id`);
                tableBuilder
                  .string('event_type')
                  .notNullable();
                tableBuilder
                  .uuid('actor_id')
                  .notNullable();
                tableBuilder
                  .uuid('repo_id')
                  .notNullable();
                tableBuilder
                  .timestamps(true, true);

                // Foreign key Constraints
                tableBuilder
                  .foreign('actor_id')
                  .references('id')
                  .inTable(`${Schema.touchCore}.${Table.actors}`);
                tableBuilder
                  .foreign('repo_id')
                  .references('id')
                  .inTable(`${Schema.touchCore}.${Table.repos}`);
              });
          }
        }))
      .catch((e) => console.error('MIGRATION_ERROR', e)));
}

// noinspection JSUnusedGlobalSymbols
export async function down(knex: Knex): Promise<void> {
  return knex.schema.withSchema(Schema.touchCore).dropTableIfExists(Table.events);
}
